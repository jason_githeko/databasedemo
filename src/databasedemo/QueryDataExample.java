/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasedemo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 
import databasedemo.MySQLConnUtils;
 
public class QueryDataExample {
 
    public static void main(String[] args) throws ClassNotFoundException,
            SQLException {
 
        // Get Connection
        Connection connection = MySQLConnUtils.getMySQLConnection();
 
        // Create statement
        Statement statement = connection.createStatement();
 
        String sql = "Select * from maneno";
 
        // Execute SQL statement returns a ResultSet object.
        ResultSet rs = statement.executeQuery(sql);
 
        // Fetch on the ResultSet        
        // Move the cursor to the next record.
        while (rs.next()) {
            String eng = rs.getString(1);
            String swa = rs.getString(2);
            System.out.println("--------------------");
            System.out.println("Eng:" + eng);
            System.out.println("Swa:" + swa);
            
        }
 
        // Close connection.
        connection.close();
    }
 
}